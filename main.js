// inpus-block

const incomeSalary = document.getElementById("income-salary"),
    incomeFreelance = document.getElementById("income-freelance"),
    incomeExtra1 = document.getElementById("income-extra-1"),
    incomeExtra2 = document.getElementById("income-extra-2");



// cost inputs

const costsFlat = document.getElementById("costs-flat"),
    costsHouseServices = document.getElementById("costs-house-services"),
    costsTransport = document.getElementById("costs-transport"),
    costsCredit = document.getElementById("costs-credit");


// total inputs

const totalMonthInput = document.getElementById("total-month"),
    totalDayInput = document.getElementById("total-day"),
    totalYearInput = document.getElementById("total-year");


let totalMonth, totalDay, totalYear;

// range

const moneyBoxRange = document.getElementById("money-box-range"),
    accumulationInput = document.getElementById("accumulation"),
    spend = document.getElementById("spend");


let totalPrecent = 0;
let accumulation = 0;


const inputs = document.querySelectorAll('.input');
for (input of inputs) {
    input.addEventListener('input', () => {
        moneySum();
        calculationPresents();

    })
}

const strToNum = str => str.value ? parseInt(str.value) : 0

const moneySum = () => {
    const sumMonth = strToNum(incomeSalary) + strToNum(incomeFreelance) + strToNum(incomeExtra1) + strToNum(incomeExtra2);

    const sumMonthCosts = strToNum(costsFlat) + strToNum(costsHouseServices) + strToNum(costsTransport) + strToNum(costsCredit);

    totalMonth = sumMonth - sumMonthCosts;
    totalMonthInput.value = totalMonth;
}

moneyBoxRange.addEventListener('input', e => {
    const totalPresentEl = document.getElementById("total-precents");
    totalPrecent = e.target.value;
    totalPresentEl.innerHTML = totalPrecent;
    calculationPresents();
})

const calculationPresents = () => {
    accumulation = ((totalMonth * totalPrecent) / 100).toFixed();
    accumulationInput.value = accumulation;

    spend.value = totalMonth - accumulation;
    totalDay = (spend.value / 30).toFixed();
    totalDayInput.value = totalDay;

    totalYear = accumulation * 12;
    totalYearInput.value = totalYear;
}

